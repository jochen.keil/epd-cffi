#!/usr/bin/env python

import os
import sys

from setuptools import setup, find_packages

src_dir = 'src'

setup(
  name='epd-cffi',
  version='0.0.1',
  description="Python FFI bindings for Waveshare's C libraries",
  long_description=open('README.md', 'rt').read(),
  url='https://gitlab.com/jochen.keil/epd-cffi',
  # https://stackoverflow.com/q/14459828
  project_urls={
    # 'Documentation': '',
    # 'Funding': '',
    # 'Say Thanks!': '',
    'Tracker': 'https://gitlab.com/jochen.keil/epd-cffi/issues',
    'Source': 'https://gitlab.com/jochen.keil/epd-cffi',
    },
  author='Jochen Keil',
  author_email='jochen.keil@gmail.com',
  classifiers=[
    'Programming Language :: Python :: 3',
    'License :: OSI Approved :: MIT License',
  ],
  packages=find_packages(where=src_dir),
  package_dir={'': src_dir},
  install_requires=['cffi>=1.0.0', 'Pillow>=1.0.0'],
  setup_requires=['cffi>=1.0.0'],
  cffi_modules=[
    'src/EPD_2in13_V2_build.py:ffibuilder'
  ],
  )
