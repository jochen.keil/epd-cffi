from enum import IntEnum

class Update(IntEnum):
    Full = 0
    Partial = 1
