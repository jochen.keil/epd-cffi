from PIL import Image
from .update import Update
from _EPD_2in13_V2_cffi import ffi, lib as libEPD

class EPD_2in13_V2:
    def __init__(self, init=False):
        self.__width = 122
        self.__height = 250
        self.__initialized = False

        if init:
            self.init(Update.Full)

    def __del__(self):
        if self.__initialized:
            self.exit()

    def width(self):
        return self.__width

    def height(self):
        return self.__height

    def initialized(self):
        return self.__initialized

    def init(self, update: Update):
        if libEPD.DEV_Module_Init() != 0:
            raise RuntimeError
        libEPD.EPD_2IN13_V2_Init(update)
        self.__initialized = True

    def to_cdata(self, buf):
        return ffi.from_buffer(buf)

    def getbuffer(self, image: Image) -> bytearray:
        '''converts a PIL.Image to bytearray'''
        img = image.convert('1').transpose(Image.FLIP_LEFT_RIGHT)
        img_width, img_height = img.size

        if img_width == self.height() and img_height == self.width():
            img = img.rotate(270, expand=True)

        return bytearray(img.tobytes('raw'))

    def displayFromBytes(self, buf: bytearray):
        libEPD.EPD_2IN13_V2_Display(self.to_cdata(buf))

    def display(self, image: Image):
        '''image: PIL.Image'''
        self.displayFromBytes(self.getbuffer(image))

    def displayPartialFromBytes(self, buf: bytearray):
        libEPD.EPD_2IN13_V2_DisplayPart(self.to_cdata(buf))

    def displayPartial(self, image: Image):
        '''image: PIL.Image'''
        self.displayPartialFromBytes(self.getbuffer(image))

    def displayPartBaseImageFromBytes(self, buf: bytearray):
        libEPD.EPD_2IN13_V2_DisplayPartBaseImage(self.to_cdata(buf))

    def displayPartBaseImage(self, image: Image):
        '''image: PIL.Image'''
        self.displayPartBaseImageFromBytes(self.getbuffer(image))

    def reset(self):
        libEPD.EPD_2IN13_V2_Reset()

    def clear(self):
        libEPD.EPD_2IN13_V2_Clear()

    def sleep(self):
        libEPD.EPD_2IN13_V2_Sleep()

    def exit(self):
        self.sleep()
        libEPD.DEV_Delay_ms(100)
        libEPD.DEV_Module_Exit()
        self.__initialized = False
