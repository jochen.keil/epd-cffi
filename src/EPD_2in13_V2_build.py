import os.path as p
from cffi import FFI

rdir = p.abspath(p.dirname(p.dirname(p.abspath(__file__))))
ldir = p.abspath(p.join(rdir, 'e-Paper', 'RaspberryPi_JetsonNano', 'c', 'lib'))
cdir = p.join(ldir, 'Config')
edir = p.join(ldir, 'e-Paper')

ffibuilder = FFI()

ffibuilder.cdef('''
    void DEV_Digital_Write(uint16_t Pin, uint8_t Value);
    uint8_t DEV_Digital_Read(uint16_t Pin);
    void DEV_SPI_WriteByte(uint8_t Value);
    void DEV_SPI_Write_nByte(uint8_t *pData, uint32_t Len);
    void DEV_Delay_ms(uint32_t xms);
    uint8_t DEV_Module_Init(void);
    void DEV_Module_Exit(void);

    void EPD_2IN13_V2_Init(uint8_t Mode);
    void EPD_2IN13_V2_Clear(void);
    void EPD_2IN13_V2_Display(uint8_t *Image);
    void EPD_2IN13_V2_DisplayPart(uint8_t *Image);
    void EPD_2IN13_V2_DisplayPartBaseImage(uint8_t *Image);
    void EPD_2IN13_V2_Sleep(void);
''')

ffibuilder.set_source('_EPD_2in13_V2_cffi',
'''
     #include "DEV_Config.h"
     #include "EPD_2in13_V2.h"
''',
    sources=[ p.join(cdir, 'DEV_Config.c')
            , p.join(edir, 'EPD_2in13_V2.c')
            ],
    libraries=['wiringPi'],
    extra_compile_args=[ '-D RPI', '-D USE_WIRINGPI_LIB', f'-I{cdir}', f'-I{edir}'])

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)
