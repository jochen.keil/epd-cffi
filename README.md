# Python CFFI bindings for Waveshare's e-Paper displays

Python FFI bindings for [Waveshare's C libraries](https://github.com/waveshare/e-Paper)

Note: This is an unofficial project, completely independent from Waveshare!

## Advantages over the native Python library:

- Performance boost (up to 20x)
- No flicker on partial refresh

## Available Modules

- EPD_2in13_V2

## Usage

### Building

```bash
# git clone --recurse-submodules https://gitlab.com/jochen.keil/epd-cffi.git
# cd epd-cffi
# python3 -m venv env
# source env/bin/activate
# pip install build
# python3 -m build
```

### Development

```bash
# git clone --recurse-submodules https://gitlab.com/jochen.keil/epd-cffi.git
# cd epd-cffi
# python3 -m venv env
# source env/bin/activate
# cd src
# python3 EPD_2in13_V2_build.py # or any EPD build file
```

### Using the library in Python

Change to your project, or wherever you want to use the library.

```bash
# pip install /path/to/epd-cffi/dist/epd_cffi-${version}-cp37-cp37m-linux_armv6l.whl
```

#### Use the wrapper:

```python
from epd import Update, EPD_2in13_V2 as EPD
epd = EPD()
epd.init(Update.Full)
epd.clear()
epd.exit()
```

#### Or call the library directly:

```python
# libEPD exposes all functions declared in the corresponding build file
from _EPD_2in13_V2_cffi import lib as libEPD

# CFFI exposes a set of helpful functions, e.g. for transforming data
# Check the CFFI docs for more information
from _EPD_2in13_V2_cffi import ffi
```

#### Examples

Check out the [examples](src/examples/) and [Waveshare's
documentation](https://www.waveshare.com/wiki/Main_Page) for more information on
how to use the libraries.
