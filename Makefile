all:
	python3 -m build

clean:
	rm -rf \
		dist \
		build \
		src/*.c \
		src/*.o \
		src/*.so \
		src/*.egg-info

.PHONY: clean
